import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { SuperHeroService } from './super-heros/superHero.service';
import { SuperHerosComponent } from './super-heros/super-heros.component';
import { DetailHeroComponent } from './super-heros/detail-hero.component';



const routes: Routes = [
  { path: '', redirectTo: '/super-heros', pathMatch: 'full' },
  { path: 'super-heros', component: SuperHerosComponent},
  { path: 'hero/detail/:id', component: DetailHeroComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    SuperHerosComponent,
    DetailHeroComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [SuperHeroService],
  bootstrap: [AppComponent]
})
export class AppModule { }
