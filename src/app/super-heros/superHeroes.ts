import {Biography} from './biography';

export class SuperHero {

    id: number;
    name: string;
    biography: Biography;
}
