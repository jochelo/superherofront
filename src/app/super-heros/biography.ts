
export class Biography {

    fullName: string;
    birthPlace: string;
    firstAppearance: string;
    publisher: string;
    alignment: string;
}
