import { Injectable } from '@angular/core';
import { SuperHero } from './superHeroes.js';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class SuperHeroService {

  private urlEndPoint: string = 'http://localhost:8080/api';
  private headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor( private http:HttpClient   ) { }

  search(searched): Observable<SuperHero[]> {
    return this.http.get<SuperHero[]>(`${this.urlEndPoint}/searchSuperHero/${searched}`,{headers : this.headers});
  }

  getById(id): Observable<SuperHero> {
    return this.http.get<SuperHero>(`${this.urlEndPoint}/getSuperHeroById/${id}`,{headers : this.headers});
  }


}
