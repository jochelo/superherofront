import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SuperHeroService } from './superHero.service';
import { SuperHero } from './superHeroes';



@Component({
    selector: 'app-form',
    templateUrl: './detail-hero.component.html'
})
export class DetailHeroComponent implements OnInit {

    private hero: SuperHero;
    private titulo: string = 'Detalle';

    constructor(
        private router: Router,
        private activateRoute: ActivatedRoute,
        private superHeroService: SuperHeroService
    ) { }

    ngOnInit() {
        this.getHero()
    }

    getHero(): void {
        console.log(this.hero);
        this.activateRoute.params.subscribe(
            params => {
                let id = params['id']
                console.log(id);
                if (id) {
                    this.superHeroService.getById(id).subscribe(
                        (hero) => { this.hero = hero }
                    )
                }
            }
        )
    }


}
