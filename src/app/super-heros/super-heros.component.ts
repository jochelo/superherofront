import { Component, OnInit } from '@angular/core';
import { SuperHeroService } from './superHero.service';
import { SuperHero } from './superHeroes';

@Component({
  selector: 'app-super-heros',
  templateUrl: './super-heros.component.html'
})
export class SuperHerosComponent implements OnInit {


  superHeros: SuperHero[];
  searched: String;

  constructor(private superHeroService: SuperHeroService) { }

  ngOnInit( ) {
  }

  search(): void {
    this.superHeroService.search(this.searched).subscribe(
        (superHeros) => { this.superHeros = superHeros }
    )
    }
}
